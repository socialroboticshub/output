package org.bitbucket.socialroboticshub;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.CapabilityType;

import io.github.bonigarcia.wdm.WebDriverManager;

public final class SICbrowser extends SICdevice {
	private final ChromeDriver driver;

	public static void main(final String... args) {
		try {
			final String[] info = getConnectionInformation();
			final SICbrowser browser = new SICbrowser(info[0], info[1], info[2]);
			browser.run();
		} catch (final Exception e) {
			SICdevice.showError(e);
			System.exit(-1);
		}
	}

	private SICbrowser(final String server, final String user, final String pass) throws Exception {
		super(server, user, pass, null);

		WebDriverManager.chromedriver().setup();

		final ChromeOptions options = new ChromeOptions();
		if (this.isLocal) {
//			options.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			options.setCapability(CapabilityType.ACCEPT_INSECURE_CERTS, true);
			options.addArguments("autoplay-policy=no-user-gesture-required");
		}
		this.driver = new ChromeDriver(options);
	}

	public void run() {
		final String url = "https://" + this.server + ":11880/index.html";
		this.driver.navigate().to(url);
	}
}
