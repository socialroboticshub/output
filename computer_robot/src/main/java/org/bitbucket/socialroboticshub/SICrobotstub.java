
package org.bitbucket.socialroboticshub;

import java.awt.BorderLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

public final class SICrobotstub extends SICdevice {
	private static final String[] topics = { "action_gesture", "action_eyecolour", "action_earcolour",
			"action_headcolour", "action_idle", "action_turn", "action_turn_small", "action_wakeup", "action_rest",
			"action_set_breathing", "action_posture", "action_stiffness", "action_play_motion", "action_motion_file",
			"action_record_motion", "action_led_color", "action_led_animation" };
	private final JLabel lastActions;

	public static void main(final String... args) {
		try {
			final String[] info = getConnectionInformation();
			final SICrobotstub speaker = new SICrobotstub(info[0], info[1], info[2]);
			speaker.run();
		} catch (final Exception e) {
			SICdevice.showError(e);
			System.exit(-1);
		}
	}

	private SICrobotstub(final String server, final String user, final String pass) throws Exception {
		super(server, user, pass, "robot");

		final JFrame window = new JFrame("SIC Robot Stub");
		window.setLayout(new BorderLayout());
		final JButton identifier = new JButton(this.identifier);
		identifier.setEnabled(false);
		window.add(identifier, BorderLayout.NORTH);

		this.lastActions = new JLabel("");
		window.add(this.lastActions, BorderLayout.WEST);

		showWindow(window);
	}

	private void setAction(final String action) {
		System.out.println(action);
		String newlabel = action + ", " + this.lastActions.getText();
		if (newlabel.length() > 65) {
			newlabel = newlabel.substring(0, 65) + "...";
		}
		this.lastActions.setText(newlabel);
	}

	public void run() throws Exception {
		try (final Jedis redis = connect()) {
			System.out.println("Subscribing '" + this.identifier + "' to " + this.server);
			redis.subscribe(new JedisPubSub() {
				@Override
				public void onMessage(final String channel, final String message) {
					new Thread(() -> {
						switch (getChannelWithoutId(channel)) {
						case "action_gesture":
							publishEvent("GestureStarted");
							setAction("Gesture: " + message);
							publishEvent("GestureDone");
							break;
						case "action_eyecolour":
							publishEvent("EyeColourStarted");
							setAction("EyeColour: " + message);
							publishEvent("EyeColourDone");
							break;
						case "action_earcolour":
							publishEvent("EarColourStarted");
							setAction("EarColour: " + message);
							publishEvent("EarColourDone");
							break;
						case "action_headcolour":
							publishEvent("HeadColourStarted");
							setAction("HeadColour: " + message);
							publishEvent("HeadColourDone");
							break;
						case "action_idle":
							setAction("Idle: " + message);
							if (message.equals("true") || message.equals("straight")) {
								publishEvent("SetIdle");
							} else {
								publishEvent("SetNonIdle");
							}
							break;
						case "action_turn":
							publishEvent("TurnStarted");
							setAction("Turn: " + message);
							publishEvent("TurnDone");
							break;
						case "action_turn_small":
							publishEvent("SmallTurnStarted");
							setAction("SmallTurn: " + message);
							publishEvent("SmallTurnDone");
							break;
						case "action_wakeup":
							publishEvent("WakeUpStarted");
							setAction("WakeUp");
							publishEvent("WakeUpDone");
							break;
						case "action_rest":
							publishEvent("RestStarted");
							setAction("Rest");
							publishEvent("RestDone");
							break;
						case "action_set_breathing":
							setAction("SetBreathing: " + message);
							final String[] split2 = message.split(";");
							if (split2[1] == "0") {
								publishEvent("BreathingDisabled");
							} else {
								publishEvent("BreathingEnabled");
							}
							break;
						case "action_posture":
							publishEvent("GoToPostureStarted");
							setAction("GoToPosture: " + message);
							publishEvent("GoToPostureDone");
							break;
						case "action_stiffness":
							publishEvent("SetStiffnessStarted");
							setAction("SetStiffness: " + message);
							publishEvent("SetStiffnessDone");
							break;
						case "action_play_motion":
						case "action_motion_file":
							publishEvent("PlayMotionStarted");
							setAction("PlayMotion: " + message);
							publishEvent("PlayMotionDone");
							break;
						case "action_record_motion":
							setAction("RecordMotion: " + message);
							if (message.startsWith("start")) {
								publishEvent("RecordMotionStarted");
							} else {
								publishEvent("RecordMotionDone");
							}
							break;
						case "action_relay_motion":
							setAction("RelayMotion: " + message);
							if (message.startsWith("start")) {
								publishEvent("RelayMotionStarted");
							} else {
								publishEvent("RelayMotionDone");
							}
							break;
						case "action_led_color":
							publishEvent("LedColorStarted");
							setAction("LedColor: " + message);
							publishEvent("LedColorDone");
							break;
						case "action_led_animation":
							publishEvent("LedAnimationStarted");
							setAction("LedAnimation: " + message);
							if (message.startsWith("start")) {
								publishEvent("LedAnimationStarted");
							} else {
								publishEvent("LedAnimationDone");
							}
							break;
						}
					}).start();
				}
			}, getTopicsWithId(topics));
		}
	}
}
