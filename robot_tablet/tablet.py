from argparse import ArgumentParser

from qi import Application


class Tablet():
    def __init__(self, session, server):
        print('Going to connect to the ALTabletService; this will always print a false "Connection refused" exception!')
        tablet_service = session.service('ALTabletService')
        tablet_service.resetTablet()
        tablet_service.enableWifi()

        url = 'https://' + server + ':11880/index.html'
        tablet_service.showWebview(url)


if __name__ == '__main__':
    parser = ArgumentParser()
    parser.add_argument('--server', type=str, help='Server IP address')
    args = parser.parse_args()

    my_name = 'Tablet'
    try:
        app = Application([my_name])
        app.start()  # initialise
        module = Tablet(session=app.session, server=args.server)
        # session_id = app.session.registerService(name, module)
        app.run()  # blocking
        # app.session.unregisterService(session_id)
    except Exception as err:
        print('Cannot connect to Naoqi: ' + err.message)
    finally:
        exit()
