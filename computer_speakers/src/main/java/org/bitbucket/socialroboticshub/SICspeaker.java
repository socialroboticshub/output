
package org.bitbucket.socialroboticshub;

import java.awt.BorderLayout;
import java.awt.Color;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import com.harium.hci.espeak.Voice;

import redis.clients.jedis.BinaryJedisPubSub;
import redis.clients.jedis.Jedis;

public final class SICspeaker extends SICdevice {
	private static final String[] topics = { "audio_language", "action_say", "action_say_animated", "action_play_audio",
			"action_stop_talking", "action_load_audio", "action_clear_loaded_audio", "action_speech_param" };
	private final String espeak;
	private final Voice voice;
	private final JLabel status;
	private final JLabel language;
	private transient Process current;
	private final List<byte[]> cache;

	public static void main(final String... args) {
		final String os = System.getProperty("os.name").toLowerCase();
		final String espeak = os.startsWith("win") ? "C:\\Program Files (x86)\\eSpeak\\command_line" : "/usr/local/bin";
		String espeakpath = (args.length > 0) ? args[0] : espeak;
		if (!os.startsWith("win") && !os.startsWith("lin") && !new File(espeakpath + "/espeak").exists())
			espeakpath = "/opt/homebrew/bin";
		try {
			final String[] info = getConnectionInformation();
			final SICspeaker speaker = new SICspeaker(info[0], info[1], info[2], espeakpath);
			speaker.run();
		} catch (final Exception e) {
			SICdevice.showError(e);
			System.exit(-1);
		}
	}

	private SICspeaker(final String server, final String user, final String pass, final String espeak)
			throws Exception {
		super(server, user, pass, "speaker");
		this.espeak = espeak;
		this.voice = new Voice();
		this.cache = Collections.synchronizedList(new ArrayList<>());

		final JFrame window = new JFrame("SIC Speaker");
		window.setLayout(new BorderLayout());
		final JButton identifier = new JButton(this.identifier);
		identifier.setEnabled(false);
		window.add(identifier, BorderLayout.NORTH);

		this.status = new JLabel();
		toggleStatus(false);
		window.add(this.status, BorderLayout.WEST);
		this.language = new JLabel("");
		window.add(this.language, BorderLayout.EAST);

		showWindow(window);
	}

	private void toggleStatus(final boolean playing) {
		if (playing) {
			this.status.setText("Playing audio!");
			this.status.setForeground(Color.GREEN);
		} else {
			this.status.setText("Not playing audio...");
			this.status.setForeground(Color.RED);
		}
	}

	public void run() throws Exception {
		try (final Jedis redis = connect()) {
			redis.ping();
			System.out.println("Subscribing '" + this.identifier + "' to " + this.server);
			redis.subscribe(new BinaryJedisPubSub() {
				@Override
				public void onMessage(final byte[] channel, final byte[] message) {
					new Thread(() -> {
						switch (getChannelWithoutId(new String(channel, UTF8))) {
						case "audio_language":
							final String language = new String(message, UTF8);
							System.out.println("AudioLanguage: " + language);
							setLanguage(language);
							break;
						case "action_say":
						case "action_say_animated":
							final String text = new String(message, UTF8);
							System.out.println("Say: " + text);
							while (SICspeaker.this.current != null) {
								try {
									Thread.sleep(1);
								} catch (final InterruptedException e) {
									break;
								}
							}
							say(text);
							break;
						case "action_play_audio":
							byte[] data = message;
							try {
								final int index = Integer.parseInt(new String(message, UTF8));
								data = SICspeaker.this.cache.get(index);
							} catch (final Exception ignore) {
							}
							playAudio(new ByteArrayInputStream(data));
							break;
						case "action_stop_talking":
							if (SICspeaker.this.current != null) {
								SICspeaker.this.current.destroyForcibly();
							}
							break;
						case "action_load_audio":
							publishEvent("LoadAudioStarted");
							final int index = SICspeaker.this.cache.size();
							SICspeaker.this.cache.add(message);
							publish("robot_audio_loaded", Integer.toString(index));
							publishEvent("LoadAudioDone");
							break;
						case "action_clear_loaded_audio":
							publishEvent("ClearLoadedAudioStarted");
							SICspeaker.this.cache.clear();
							publishEvent("ClearLoadedAudioDone");
							break;
						case "action_speech_param":
							publishEvent("SetSpeechParamDone");
							break;
						}
					}).start();
				}
			}, getRawTopicsWithId(topics));
		}
	}

	private void setLanguage(final String lang) {
		this.voice.setName(lang.toLowerCase());
		this.language.setText(lang);
		publishEvent("LanguageChanged");
	}

	private void say(final String rawtext) {
		final String text = rawtext.replaceAll("\\\\.+\\\\", ""); // remove any special naoqi commands from the text
		if (text.isEmpty()) { // espeak hangs otherwise
			publishEvent("TextStarted");
			publishEvent("TextDone");
			return;
		}
		final String[] command = { this.espeak + File.separator + "espeak", "-v",
				this.voice.getName() + this.voice.getVariant(), text };
		final ProcessBuilder b = new ProcessBuilder(command).inheritIO();
		try {
			toggleStatus(true);
			publishEvent("TextStarted");
			this.current = b.start();
			this.current.waitFor();
			publishEvent("TextDone");
			toggleStatus(false);
			this.current.destroy();
		} catch (final Exception e) {
			e.printStackTrace();
		} finally {
			this.current = null;
		}
	}

	private void playAudio(final InputStream audio) {
		try {
			final Clip sound = AudioSystem.getClip();
			sound.open(AudioSystem.getAudioInputStream(audio));
			toggleStatus(true);
			publishEvent("PlayAudioStarted");
			sound.start();
			while (sound.getMicrosecondPosition() < sound.getMicrosecondLength()) {
				Thread.sleep(1);
			}
			publishEvent("PlayAudioDone");
			toggleStatus(false);
			sound.close();
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}
}
