
package org.bitbucket.socialroboticshub;

import java.awt.BorderLayout;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPubSub;

public final class SIClogger extends SICdevice {
	private static final String[] topics = { "session_start", "session_log", "session_end" };
	private static final Format dateTime = new SimpleDateFormat("yyyyMMddHHmmss");
	private final Writer logWriter;
	private final JLabel lastLog;
	private String sessionId;

	public static void main(final String... args) {
		try {
			final String[] info = getConnectionInformation();
			final JFileChooser fileChooser = new JFileChooser();
			fileChooser.setDialogTitle("Log output directory");
			fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			fileChooser.showOpenDialog(null);
			final SIClogger speaker = new SIClogger(info[0], info[1], info[2], fileChooser.getSelectedFile());
			speaker.run();
		} catch (final Exception e) {
			SICdevice.showError(e);
			System.exit(-1);
		}
	}

	private SIClogger(final String server, final String user, final String pass, final File logDir) throws Exception {
		super(server, user, pass, "logger");

		final String fileName = dateTime.format(new Date()) + ".csv";
		final File logFile = new File(logDir, fileName);
		this.logWriter = new FileWriter(logFile, StandardCharsets.UTF_8);
		System.out.println("Logging to " + logFile);

		final JFrame window = new JFrame("SIC Logger");
		window.setLayout(new BorderLayout());
		final JButton identifier = new JButton(this.identifier);
		identifier.setEnabled(false);
		window.add(identifier, BorderLayout.NORTH);

		this.lastLog = new JLabel("");
		window.add(this.lastLog, BorderLayout.WEST);

		showWindow(window);
	}

	private void log(final String message) throws IOException {
		if (this.sessionId == null) {
			throw new IOException("No active session, log discarded!");
		}
		final String log = dateTime.format(new Date()) + ";" + this.sessionId + ";" + message;
		System.out.println(log);
		this.logWriter.write(log + "\n");
		this.logWriter.flush();
		if (log.length() > 65) {
			this.lastLog.setText(log.substring(0, 65) + "...");
		} else {
			this.lastLog.setText(log);
		}
	}

	public void run() throws Exception {
		final Jedis publisher = connect();
		try (final Jedis redis = connect()) {
			System.out.println("Subscribing '" + this.identifier + "' to " + this.server);
			redis.subscribe(new JedisPubSub() {
				@Override
				public void onMessage(final String channel, final String message) {
					switch (getChannelWithoutId(channel)) {
					case "session_start":
						if (SIClogger.this.sessionId == null) {
							try {
								SIClogger.this.sessionId = message;
								publisher.set(SIClogger.this.identifier + "_session_id", message);
								log("start");
							} catch (final Exception e) {
								e.printStackTrace();
							}
						} else {
							System.out.println("Session already active!");
						}
						break;
					case "session_log":
						try {
							log(message);
						} catch (final Exception e) {
							e.printStackTrace();
						}
						break;
					case "session_end":
						if (SIClogger.this.sessionId != null) {
							try {
								log("end");
								SIClogger.this.sessionId = null;
								publisher.del(SIClogger.this.identifier + "_session_id");
							} catch (final Exception e) {
								e.printStackTrace();
							}
						} else {
							System.out.println("No session active!");
						}
						break;
					}
				}
			}, getTopicsWithId(topics));
		}
	}
}
